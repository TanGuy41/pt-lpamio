/*! markdown-it-checkbox 1.2.0-4 https://github.com//GerHobbelt/markdown-it-checkbox @license MIT */

'use strict';

// Utility functions
let checkboxReplace, move;

move = function (arr, element, offset) {
  let index, newIndex, removedElement;
  index = arr.indexOf(element);

  if (index < 0) {
    // if not found, return immediately
    return index;
  }

  newIndex = index + offset;

  if (newIndex < 0) {
    newIndex = 0;
  }

  if (newIndex >= arr.length) {
    newIndex = arr.length - 1;
  } // remove the element from the array


  removedElement = arr.splice(index, 1)[0]; // at newIndex, remove 0 elements, insert the removedElement

  arr.splice(newIndex, 0, removedElement); // return the newIndex of the removedElement

  return newIndex;
}; // Checkbox replacement logic.


checkboxReplace = function (md, options, Token) {
  let arrayReplaceAt, createTokens, defaults, lastId, pattern, splitTextToken;
  arrayReplaceAt = md.utils.arrayReplaceAt;
  lastId = 0;
  defaults = {
    divWrap: false,
    divClass: 'checkbox',
    idPrefix: 'checkbox',
    readonly: false,
    disabled: false,
    customHTML: false
  };
  options = md.utils.assign(defaults, options);
  pattern = /\[(X|\s|\_|\-)\]\s(.*)/i;

  createTokens = function (checked, label, Token) {
    /**
     * <input type="checkbox" id="checkbox{n}" checked="" readonly="">
     */
    let attr, customHTML, getTag, id, inputTag, labelTag, newInputTag, newLabelTag, nodes, token;
    nodes = [];
    id = options.idPrefix + lastId;
    lastId += 1;

    if (options.customHTML) {
      token = new Token('html_inline', '', 0);
      customHTML = options.customHTML;

      getTag = function (str, tagName, content = '') {
        let matches, regexp, res;
        regexp = new RegExp(`(<${tagName}.*?>)(${content})?`, 'igm');
        res = regexp.exec(str);

        if (!res) {
          return res;
        }

        matches = regexp.exec(str);

        while (matches) {
          if (~matches.indexOf(content) && content.length) {
            res = matches;
          }

          matches = regexp.exec(str);
        }

        return res[0];
      };

      attr = function (tag, attributes = {}) {
        let addAttr, attrName, attrRegexp, attrValue, replaceAttr;

        replaceAttr = function (regexp, value) {
          return tag.replace(regexp, `$1${value}$2`);
        };

        addAttr = function (attr, val) {
          let regexp, tagName;
          tagName = tag.match(/<(\w+?)\s/)[1];
          regexp = new RegExp(`(<${tagName})(.+?>)`);

          if (val === true) {
            val = '';
          }

          return tag.replace(regexp, `$1 ${attr}=\"${val}\"$2`);
        };

        for (attrName in attributes) {
          attrValue = attributes[attrName];
          attrRegexp = new RegExp(`(${attrName}=['\"]).+?(['\"])`);

          if (attrValue === false) {
            continue;
          }

          if (~tag.search(attrRegexp)) {
            tag = replaceAttr(attrRegexp, attrValue);
          } else {
            tag = addAttr(attrName, attrValue);
          }
        }

        return tag;
      };

      labelTag = getTag(customHTML, 'label', '{label}');
      inputTag = getTag(customHTML, 'input');

      if (labelTag) {
        newLabelTag = attr(labelTag, {
          'for': id
        });

        if (~newLabelTag.search('{label}')) {
          newLabelTag = newLabelTag.replace('{label}', label);
          customHTML = customHTML.replace(labelTag, newLabelTag);
        } else {
          customHTML = customHTML.replace(/<label.*?>/, newLabelTag + label);
        }
      }

      if (inputTag) {
        newInputTag = attr(inputTag, {
          id: id,
          checked: checked,
          disabled: options.disabled
        });
        customHTML = customHTML.replace(/<input.+?>/, newInputTag);
      }

      token.content = customHTML;
      nodes.push(token);
      return nodes;
    }
    /**
     * <div class="checkbox">
     */


    if (options.divWrap) {
      token = new Token('checkbox_open', 'div', 1);
      token.attrs = [['class', options.divClass]];
      nodes.push(token);
    }

    token = new Token('checkbox_input', 'input', 0);
    token.attrs = [['type', 'checkbox'], ['id', id]];

    if (checked === true) {
      token.attrs.push(['checked', '']);
    }

    if (options.readonly) {
      token.attrs.push(['readonly', '']);
    }

    if (options.disabled) {
      token.attrs.push(['disabled', '']);
    }

    nodes.push(token);
    /**
     * <label for="checkbox{n}">
     */

    token = new Token('label_open', 'label', 1);
    token.attrs = [['for', id]];
    nodes.push(token);
    /**
     * content of label tag
     */

    token = new Token('text', '', 0);
    token.content = label;
    nodes.push(token);
    /**
     * closing tags
     */

    nodes.push(new Token('label_close', 'label', -1));

    if (options.divWrap) {
      nodes.push(new Token('checkbox_close', 'div', -1));
    }

    return nodes;
  };

  splitTextToken = function (original, Token) {
    let checked, label, matches, text, value;
    text = original.content;
    matches = text.match(pattern);

    if (matches === null) {
      return original;
    }

    checked = false;
    value = matches[1];
    label = matches[2];

    if (value === 'X' || value === 'x') {
      checked = true;
    }

    return createTokens(checked, label, Token);
  };

  return function (state) {
    let blockTokens, i, j, k, l, labelClose, labelOpens, len, mappedTokens, open, ref, ref1, suitableIdx, token, tokens;
    blockTokens = state.tokens;
    j = 0;
    l = blockTokens.length;

    while (j < l) {
      if (blockTokens[j].type !== 'inline') {
        j++;
        continue;
      }

      tokens = blockTokens[j].children; // We scan from the end, to keep position when new tags added.
      // Use reversed logic in links start/end match

      i = tokens.length - 1;

      while (i >= 0) {
        token = tokens[i];
        blockTokens[j].children = tokens = arrayReplaceAt(tokens, i, splitTextToken(token, state.Token));
        i--;
      }

      j++;
    }

    j = 0;
    l = blockTokens.length;

    while (j < l) {
      if (blockTokens[j].type !== 'inline') {
        j++;
        continue;
      }

      tokens = blockTokens[j].children;
      mappedTokens = tokens.map(function (t, idx) {
        return {
          idx: idx,
          type: t.type,
          token: t
        };
      });
      suitableIdx = tokens.length - 1;
      labelOpens = mappedTokens.filter(function (t) {
        return t.type === 'label_open';
      });
      ref = labelOpens.reverse();

      for (k = 0, len = ref.length; k < len; k++) {
        open = ref[k];

        if (suitableIdx < 0) {
          suitableIdx = 0;
        }

        while ((ref1 = mappedTokens[suitableIdx].type) === 'softbreak' || ref1 === 'checkbox_close') {
          suitableIdx--;
        }

        labelClose = mappedTokens.find(function (t, idx) {
          return open.idx < idx && idx <= suitableIdx && t.type === 'label_close';
        });
        move(tokens, labelClose.token, suitableIdx - labelClose.idx);
        suitableIdx = open.idx - 2;
      }

      j++;
    }
  };
};

function index (md, options) {
  md.core.ruler.push('checkbox', checkboxReplace(md, options));
} //# sourceURL=Z:\lib\js\markdown-it-checkbox\index.coffee

module.exports = index;
//# sourceMappingURL=markdownItCheckbox.cjs.map
