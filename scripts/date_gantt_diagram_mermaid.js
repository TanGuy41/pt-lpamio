async function renderTickText()
{
    const dateTexts = document.querySelectorAll(".mermaid > svg .grid .tick > text");
    for (dateTxt of dateTexts)
    {
        dateTxt.attributes.removeNamedItem("fill");
        dateTxt.style.fill="var(--mermaid-gantt-tickTextColor)";
    }
}