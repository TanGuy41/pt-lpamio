---
layout: layouts/page.njk
---

# Cahier des Charges Technique

## Schéma des Entités / Schéma de la BD

```mermaid
%%{init: {'theme': 'neutral','themeVariables': {
    'darkMode': 'var(--mermaid-dark)',
    'background': 'var(--bg-color)'
}}}%%
classDiagram
class  User {
    - uuid
    - nom
    - email
    - password
    - roles
    - is_verified
}
class Livraison {
    - uuid_tournee
    - uuid_client
    - effectuee
    - date_heure
    - adresse
}
class Tournee {
    - uuid
    - uuid_gestionnaire
    - date
    - heure_depart
    - heure_fin?
    - lieu_depart
}

Tournee --* Livraison : est composé de
Livraison --> User : (si user est un client) est associé à
Tournee --> User : (si user est un gestionnaire) est gérer par 


```

## Technologies utilisées

- Framework PHP Symfony 5
- PHP 7.3
- Leaflet
- Framework CSS / JS Bootstrap 5
- MariaDB / MySQL
- Docker
- Docker Compose


