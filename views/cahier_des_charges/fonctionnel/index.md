---
layout: layouts/page.njk
---

# Cahier des Charges Fonctionnel

## Demande du Client

Notre client (un restaurant de burger) a besoin d'une apllication ainsi que d'un système connectée permettant de gérer ses clients et leurs livraisons. Il souhaite avoir la possibilité de vérifier que la température a été respectée tout au long d'une livraison, et s'assurer qu'il n'y a eu aucun choque en cours de livraison. Il souhaite aussi pouvoir connaitre l'itinéraire utilisé par les livreurs.

Pour que les clients puisse etre livré chez eux peu importe la taille du burger, le client shoutaite développer une boité à burger partagée.


## Descriptif des fonctionnalités

L'application devra permettre à l'utilisateur :

- Créer, Modifier, Suprimer un client ou l'une de ses adresses.
- Suivre les tournée de livraisns en temps réel, connaître la position des différents livreurs, le nombre de livraison effectuées et restante toatale et par livreur.
- Ajouter ou retirer une livraison d'une tournée
- Créer une tournée manuellement avec des suggestions d'iténiraire, ou bien de manière automatique (toutes les x commandes).
- Modifier une livraisons (addresse, date, ...)
- Générer les documents concernant une livraisons (étiquete du colis, reçu, ...)

```mermaid
%%{init: {'theme': 'neutral'}}%%
flowchart LR

user([Gestionnaire])
client([Client])
subgraph Système
    manage_account(Gérer son compte)
    create_account(Créer un compte)
    delete_account(Supprimer son compte)
    edit_account(Modifier son compte)
    manage_account-->delete_account
    manage_account-->edit_account
    edit_account-->edit_client_address

    manage_client(Gérer les Clients)
    create_client(Créer des Clients)
    edit_client(Modifier des clients)
    delete_client(Supprimer des clients)
    manage_client-->create_client
    manage_client-->edit_client
    manage_client-->delete_client

    edit_client_address(Gérer les Adresses du Client)
    add_address(Ajouter une ou plusieurs adresses)
    edit_address(Modifier une ou plusieurs adresses)
    delete_address(Supprimer une ou plusieurs adresses)
    edit_client_address-->add_address
    edit_client_address-->edit_address
    edit_client_address-->delete_address
    edit_client-->edit_client_address
end

user-->manage_client
client-->create_account
client-->manage_account
```

```mermaid
%%{init: {'theme': 'neutral'}}%%
flowchart LR

user([Gestionnaire])
client([Client])
subgraph Système
    manage_delivery(Gérer les livraisons)
    gen_delivery_doc(Générer les documents concernant une livraisons)
    change_delivery(Modifier la livraison)
    manage_delivery-->gen_delivery_doc
    manage_delivery-->change_delivery

    gen_parcours(Générer les tournées)
    gen_auto(Générer automatiquement)
    gen_manual(Générer manuellement)
    config_auto(Configurer le generation automatique des iténiaires)
    gen_parcours-->gen_manual
    gen_parcours-->gen_auto
    gen_auto-->config_auto

    manage_deliveries(Gérer les tournées)
    get_all_info(Connître l'avncée globale des tournées)
    get_deliveries_info(Connaître l'avancée d'une tournée)
    add_delivery(Ajouter une livraison à une tournée)
    delete_delivery(Supprimer une lvraison à une tournée)
    manage_deliveries-->add_delivery
    manage_deliveries-->delete_delivery
    manage_deliveries-->get_all_info
    manage_deliveries-->get_deliveries_info

end

user-->manage_delivery
client-->manage_delivery
user-->manage_deliveries
user-->gen_parcours
```


## Etude Concurentielle

| Concurrent | + | - | prix |
|:--------|------|------|----:|
| myDPD for business / myDPD Local (dpdgroup) | <ul><li>Très très complet</li><li>Gestion clients et gestion livraisons</li><li>Posssibilité de créer de documents administratif ou personnalisé</li><li>Importation des commandes depuis des site d'e-commerce au format CSV</li><li>Intégration avec CRM, ERP, ...</li></ul> | <ul><li>est uniquement utilisable avec des colis du groupe DPD</li></ul> | ? |
| ShipRush | <ul><li>intégration avec plusieurs transporteurs</li><li>S'intégre avec des boutiques en ligne</li><li>Intégration avec CRM, ERP, ...</li><li>Génération de documents (étiquettes, ...)</li></ul> | <ul><li>Orienté e-commerce propose donc beaucoup de fonctionnlités en lien avec mais qui n'intéreese pas notre client</li><li>Pas de gestion des tournées</li></ul> | à partir de 29,95 USD / mois |
| ShipStation | <ul><li>intégration avec plusieurs transporteurs</li><li>Intégration avec des boutiques en ligne</li><li>Génération de documents (étiquettes, ...)</li></ul> | <ul><li>Orienté e-commerce propose donc beaucoup de fonctionnlités en lien avec mais qui n'intéreese pas notre client</li><li>Pas de gestion des tournées</li><li>Pas d'intégration CRM, ERP</li></ul> | à partir de 25 € / mois |
| Metapack | <ul><li>intégration avec plusieurs transporteurs</li><li>Intégration avec des boutiques en ligne</li></ul> | <ul><li>Orienté e-commerce propose donc beaucoup de fonctionnlités en lien avec mais qui n'intéreese pas notre client</li><li>Pas de gestion des tournées</li><li>Pas d'intégration CRM, ERP</li></ul> | ? |
| AntsRoute | <ul><li>Optimisation des tournées</li><li>Suivi de livraison en temps réel</li><li>Intégration avec applis, API REST</li></ul> | <ul><li>Forfait par véhicule</li><li></li></ul> | à partir de 44 € / mois / véhicule|
## Devis

smic brut : 12 € / h

nombre d'heure de travail par jour : 7

nombre de jour de travail par semainde : 5

donc semaine de  35 heures 

coût d'une semaine de travail = 420 €

durée totale du projet en jours = 102 jours

durée totale du projet en semaines = 20 semaines

nombre de personnes sur le projet = 2 donc quantité à facturer 2*20 = 40

|  | Qte | Prix Unité | Total |
|---|------:|------:|------:|
| Semaines de travail | 40  | 420 € | 16.800 € |
| Serveur | 1 | 400 € | 400 € |
| Total H.T | | | 17.200 € |

<br/>

| Taxe | Taux | Total |
|----|----|-----:|
| TVA | 20% | 3.440 € |
| Total | | 3.440 € |

<br/>

| | Total |
|:---|----:|
| Total H.T | 17.200 € |
| Total taxes | 3.440 € |
| Total TTC | 20.640 € |

## Planning prévisionnel

```mermaid
gantt
    %%{init: {'theme': 'forest', 'themeVariables': {
        'titleColor': 'var(--mermaid-gantt-sequenceTitleColor)',
        'taskTextOutsideColor': 'var(--mermaid-gantt-taskTextOutsideColor)',
        'taskTextColor': 'var(--mermaid-gantt-taskTextColor)'
    }}}%%
    dateFormat DD-MM-YYYY
    axisFormat %d-%m
    excludes weekends
    todayMarker off

    Analyse des besoins                     : analyse_des_besoins, 11-10-2021, 8d
    Descriptif des fonctionnalités          : descriptif_fonctionnalites, 11-10-2021, 14d
    Etude Concurentielle                    : etude_concurentielle, 15-10-2021, 10d
    Mise en place BD                        : setup_bd, after etude_concurentielle, 3d
    Développement de l'application          : dev_app, after etude_concurentielle, 90d
    Développement Test                      : dev_test, after etude_concurentielle, 95d
    Correctifs                              : part1_patchs, after dev_app, 10d    
    Documentation de l'application          : docs_app, after etude_concurentielle, 100d


```

