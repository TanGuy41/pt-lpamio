---
layout: layouts/page.njk
---

# Suivi de livraison

__*Tuteur*__: Emmanuel Medina

__*Etudiants*__: [Tanguy Martin](https://gitlab.com/TanGuy41), [Alban Cavali](https://github.com/AlbanFCB)

__*Mots clés*__: transport, parcours, sécurité, traçage

## Partie 1

Concevoir une application pour __gérer les clients, des tournées de livraison, le suivi de la réception des marchandises__.

[Cahier des Charges Fonctionnel](cahier_des_charges/fonctionnel)

[Cahier des Charges Technique](cahier_des_charges/technique)

[Présentation Technique](presentation_technique)
