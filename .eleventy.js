
module.exports = function(config) {
  let markdownOptions = {
    html: true
  };
  const markdownIt = require("markdown-it")(markdownOptions)
                        .use(require("markdown-it-textual-uml"))
                        .use(require('./lib/markdownItCheckbox.cjs'),{
                          divWrap: true,
                          divClass: 'cb',
                          idPrefix: 'cbx_',
                          readonly: true,
                          disabled: true,
                        });

  config.setLibrary("md", markdownIt);

  config.addPassthroughCopy({"scripts/**/*[^.11ty].js":"js/"});
  config.addPassthroughCopy({"styles/**/*.css":"css/"});

  config.addPassthroughCopy("views/**/*.png")
    .addPassthroughCopy("views/**/*.jpeg")
    .addPassthroughCopy("views/**/*.jpg")
    .addPassthroughCopy("views/**/*.svg")
    .addPassthroughCopy("views/**/*.gif");

  return {
    dir: {
      input: "views",
      output: "public"
    }
  }
};